# Как работать с текущим проектом

Подтянуть к себе проект:
```aidl
> git clone ...
> cd infrstructure/terraform/yc
```

Инициализировать провайдера:
```shell
> terraform init
```

Выбрать workspace с которым будете работать(development или production):
```shell
> terraform workspace select development
```

Проверить вносимые изменения:
```shell
> terraform plan --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

Применить изменения
```shell
> terraform apply --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```


## Создание нового кластера

* [Инструкция по установке terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* [Документация по Terraform](https://www.terraform.io/docs/language/index.html)
* [Документация по провайдеру yandex-cloud](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs)
* [Документация по созданию сервисного акаунта и ключа, для запуска terraform манифеста](https://cloud.yandex.ru/docs/iam/operations/iam-token/create-for-sa#keys-create)  
* [Документация yandex-cloud по созданию кластера k8s](https://cloud.yandex.ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-create)
* [Документация yandex-cloud по созданию групп безопасности для k8s](https://cloud.yandex.ru/docs/managed-kubernetes/operations/security-groups)
* [Документация yandex-cloud по созданию группы узлов](https://cloud.yandex.ru/docs/managed-kubernetes/operations/node-group/node-group-create)

#### Создаем файл secret.tfvars с конфигурационными переменными
```shell
> cp secret-dev.tfvars.dist secret-dev.tfvars
```
Заполняем переменные

#### Инициализация провайдера
```shell
> terraform init --var-file=secret-dev.tfvars
```
Команда создаст папку `.terraform` с ресурсами провайдера

#### Создание workspace

```shell
> terraform workspace new development
```

Переключение между рабочими окружениями:
```shell
> terraform workspace select development
```

#### Проверяем план выполнения инструкции
```shell
> terraform plan --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```
Убеждаемся что terraform не выполнит ничего лишнего

#### Запускаем выполнение инструкции
```shell
> terraform apply --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

## Обновление инфрастуркутры

Выбрать окружение(development/production):
```shell
> terraform workspace select development
```

Проверить вносимые изменения
```shell
> terraform plan --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

Применить изменения:
```shell
> terraform apply --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

## Удаление рабочей инфраструктуры

```shell
> terraform workspace select <workspace>
> terraform destroy --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```