variable "service_account_key_file" {
  description = "Path to service account key in json format"
  type = string
  sensitive = true
}

variable "cloud_id" {
  description = "Cloud id"
  type = string
  sensitive = true
}

variable "folder_id" {
  description = "Folder id"
  type = string
  sensitive = true
}

variable "zone" {
  description = "zone"
  type = string
  sensitive = true
}

variable "env" {
  description = "Environment. stand or prod"
  type = string
  default = "stand"
}

variable "k8s_version" {
  description = "Version of k8s"
  type = string
  default = "1.19"
}

variable "k8s_node_platform" {
  description = "Node platform for k8s node group"
  type = string
  default = "standard-v1"
}

variable "k8s_node_min_count" {
  description = "Min node count for auto scale"
  type = number
  default = 3
}

variable "k8s_node_max_count" {
  description = "Max node count for auto scale"
  type = number
  default = 10
}

variable "k8s_node_cpu_cores" {
  description = "Node cpu cores"
  type = number
  default = 2
}

variable "k8s_node_cpu_core_fraction" {
  description = "Node cpu core fraction"
  type = number
  default = 100
}


variable "k8s_node_memory_size" {
  description = "Node RAM size"
  type = number
  default = 8
}

variable "k8s_node_disk_size" {
  description = "Node disk size. Min 64GB"
  type = number
  default = 64
}

variable "k8s_master_whitelist" {
  description = "IP whitelist"
  type = list(string)
  default = [
    "192.168.0.0/24",
    "22.22.22.22/32"
  ]
}

variable "office_external_ips" {
  description = "Office external ip addresses"
  type = list(string)
  default = ["125.92.171.51/32", "71.124.12.112/32"]
}

variable "enable_nat_instane" {
  description = "create nat instance for outgoing traffic"
  type = bool
  default = false
}

variable "nat_public_address" {
  description = "Public ip address for nat instance"
  type = string
}

variable "nat_instance_cpu_core_fraction" {
  description = "NAT instance cpu core fraction"
  type = number
  default = 100
}