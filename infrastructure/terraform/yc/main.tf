terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.63.1"
    }
  }
}

provider "yandex" {
  service_account_key_file = var.service_account_key_file
  cloud_id = var.cloud_id
  folder_id = var.folder_id
  zone = var.zone
}

##
# Managed Service for Kubernetes - k8s кластер
##
resource yandex_kubernetes_cluster "regional_cluster_for_functions" {
  name = "otus-md-fn-${var.env}"
  description = "Cluster for ${var.env}, created via terraform"
  network_id = yandex_vpc_network.this.id

  // Диапазон IP-адресов, из которого будут выделяться IP-адреса для подов
  cluster_ipv4_range = "10.112.0.0/16"
  // Диапазон IP-адресов, из которого будут выделяться IP-адреса для сервисов.
  service_ipv4_range = "10.96.0.0/16"

  master {
    version = var.k8s_version

    ##
    # Config for regional cluster
    ##
    regional {
      region = "ru-central1"
      location {
        zone = yandex_vpc_subnet.subnet_a.zone
        subnet_id = yandex_vpc_subnet.subnet_a.id
      }
      location {
        zone = yandex_vpc_subnet.subnet_b.zone
        subnet_id = yandex_vpc_subnet.subnet_b.id
      }
      location {
        zone = yandex_vpc_subnet.subnet_c.zone
        subnet_id = yandex_vpc_subnet.subnet_c.id
      }
    }

    ##
    # Config for zonal cluster
    ##
#    zonal {
#      zone = "${yandex_vpc_subnet.subnet_a.zone}"
#      subnet_id = "${yandex_vpc_subnet.subnet_a.id}"
#    }

    public_ip = true

    security_group_ids = [
      yandex_vpc_security_group.k8s-main-sg.id,
      yandex_vpc_security_group.k8s-public-services.id,
      yandex_vpc_security_group.k8s-master-whitelist.id,
      yandex_vpc_security_group.k8s-nodes-ssh-access.id
    ]
    maintenance_policy {
      auto_upgrade = true
    }
  }

  // Сервисный аккаунт для ресурсов кластера
  service_account_id = yandex_iam_service_account.this.id
  // Сервисный аккаунт для узлов кластера
  node_service_account_id = yandex_iam_service_account.this.id
  release_channel = "REGULAR"
  // Применить сетевые политики для кластера. Будет задействован контроллер сетевой политики Calico.
  network_policy_provider = "CALICO"
  depends_on = [
    yandex_resourcemanager_folder_iam_member.editor,
    yandex_resourcemanager_folder_iam_member.images-puller
  ]
}

##
# Virtual Private Cloud(VPC) - Облачная сеть в которой будет развернут кластер
##
resource "yandex_vpc_network" "this" {
  name = "otus-md-fn-${var.env}-network"
  description = "cloud network for ${var.env} function k8s"
}

##
# VPC Subnet zone A - подсет в зоне А облачной сети
##
resource "yandex_vpc_subnet" "subnet_a" {
  name = "otus-md-fn-${var.env}-subnet-ru-central1-a"
  network_id = yandex_vpc_network.this.id
  zone = "ru-central1-a"
  v4_cidr_blocks = ["10.128.0.0/24"]
  #route_table_id = yandex_vpc_route_table.nat-route-table[0].id
}
##
# VPC Subnet zone B - подсет в зоне B облачной сети
##
resource "yandex_vpc_subnet" "subnet_b" {
  name = "otus-md-fn-${var.env}-subnet-ru-central1-b"
  network_id = yandex_vpc_network.this.id
  zone = "ru-central1-b"
  v4_cidr_blocks = ["10.129.0.0/24"]
  #route_table_id = yandex_vpc_route_table.nat-route-table[0].id
}
##
# VPC Subnet zone C - подсет в зоне C облачной сети
##
resource "yandex_vpc_subnet" "subnet_c" {
  name = "otus-md-fn-${var.env}-subnet-ru-central1-c"
  network_id = yandex_vpc_network.this.id
  zone = "ru-central1-c"
  v4_cidr_blocks = ["10.130.0.0/24"]
  #route_table_id = yandex_vpc_route_table.nat-route-table[0].id
}

##
# VPC Subnet for NAT instance
##
resource "yandex_vpc_subnet" "subnet_nat" {
  count = var.enable_nat_instane ? 1 : 0
  name = "otus-md-fn-${var.env}-subnet-nat-ru-central1-b"
  network_id = yandex_vpc_network.this.id
  zone = "ru-central1-b"
  v4_cidr_blocks = ["10.131.0.0/24"]
}

//data "yandex_compute_image" "nat-instance-image" {
//  ##
//  # https://cloud.yandex.ru/marketplace/products/yc/nat-instance-ubuntu-18-04-lts
//  ##
//  image_id = "as8312r03berbdebj48b"
//}

//resource "yandex_compute_instance" "nat_instance" {
//  count = var.enable_nat_instane ? 1 : 0
//  name = "otus-md-fn-${var.env}-nat-instance"
//  zone = "ru-central1-b"
//  platform_id = var.k8s_node_platform
//  resources {
//    memory = 4
//    cores = 2
//    core_fraction = var.nat_instance_cpu_core_fraction
//  }
//  allow_stopping_for_update = true
//  boot_disk {
//    initialize_params {
//      image_id = data.yandex_compute_image.nat-instance-image.id
//      size = 15
//      type = "network-hdd"
//    }
//  }
//
//  network_interface {
//    subnet_id = yandex_vpc_subnet.subnet_nat[0].id
//    ##
//    # register ip address - https://cloud.yandex.ru/docs/vpc/operations/get-static-ip
//    ##
//    nat_ip_address = var.nat_public_address
//    nat = true
//    security_group_ids = [yandex_vpc_security_group.nat-sg.id]
//  }
//
//  service_account_id = yandex_iam_service_account.this.id
//}

//resource "yandex_vpc_route_table" "nat-route-table" {
//  count = var.enable_nat_instane ? 1 : 0
//  network_id = yandex_vpc_network.this.id
//  name = "otus-md-fn-${var.env}-nat-route"
//  description = "Роутинг исходящего трафика через NAT инстанс"
//
//  static_route {
//    destination_prefix = "0.0.0.0/0"
//    next_hop_address = yandex_compute_instance.nat_instance[0].network_interface[0].ip_address
//  }
//}

resource "yandex_vpc_security_group" "nat-sg" {
  network_id = yandex_vpc_network.this.id
  name = "otus-md-fn-${var.env}-nat-sg"
  description = "Правила для nat инстанса"

  labels = {
    secgroup = "otus-md-fn-${var.env}"
  }

  ingress {
    protocol = "ICMP"
    description = "Правило разрешает отладочные ICMP-пакеты."
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает подключение к узлам по SSH с указанных IP-адресов."
    v4_cidr_blocks = concat(["10.0.0.0/8", "192.168.0.0/16", "172.16.0.0/12", "85.32.32.22/32"], var.office_external_ips)
    port           = 22
  }

  ingress {
    protocol = "ANY"
    predefined_target = "self_security_group"
  }

  ingress {
    protocol = "ANY"
    description    = "Правило разрешает входящий трафик из подсетей кластера"
    v4_cidr_blocks = ["10.128.0.0/24", "10.129.0.0/24", "10.130.0.0/24"]
    from_port = 0
    to_port = 65535
  }

  egress {
    protocol = "ANY"
    description = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Object Storage, Yandex Container Registry, Docker Hub и т. д."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    to_port = 65535
  }
}

/**
* Security group - Группа безопасности для облачной сети
*/
resource "yandex_vpc_security_group" "k8s-main-sg" {
  name = "otus-md-fn-${var.env}-sg-k8s-main"
  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените ее к кластеру и группам узлов."
  network_id = yandex_vpc_network.this.id

  labels = {
    secgroup = "otus-md-fn-${var.env}"
  }

  ingress {
    protocol = "TCP"
    description = "Правило разрешает проверки доступности с диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."
    v4_cidr_blocks = ["198.18.235.0/24", "198.18.248.0/24"]
    from_port = 0
    to_port = 65535
  }

  ingress {
    protocol = "ANY"
    description = "Правило разрешает взаимодействие master-to-node и node-to-node внутри группы безопасности."
    predefined_target = "self_security_group"
    from_port = 0
    to_port = 65535
  }

  ingress {
    protocol = "ANY"
    description = "Правило разрешает взаимодействие pod-to-pod и service-to-service. Укажите подсети вашего кластера и сервисов."
    v4_cidr_blocks = ["10.112.0.0/16", "10.96.0.0/16"]
  }

  ingress {
    protocol = "ICMP"
    description = "Правило разрешает отладочные ICMP-пакеты из внутренних подсетей."
    v4_cidr_blocks = ["10.0.0.0/8", "192.168.0.0/16", "172.16.0.0/12"]
  }

  egress {
    protocol = "ANY"
    description = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Object Storage, Yandex Container Registry, Docker Hub и т. д."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    to_port = 65535
  }
}

resource "yandex_vpc_security_group" "k8s-public-services" {
  name = "otus-md-fn-${var.env}-sg-k8s-public-services"
  network_id = yandex_vpc_network.this.id
  description = "Правила группы разрешают подключение к сервисам из интернета. Примените правила только для групп узлов."

  ingress {
    protocol = "TCP"
    description = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port = 30000
    to_port = 32767
  }
}

resource "yandex_vpc_security_group" "k8s-nodes-ssh-access" {
  name        = "otus-md-fn-${var.env}-sg-k8s-nodes-ssh-access"
  description = "Правила группы разрешают подключение к узлам кластера через SSH. Примените правила только для групп узлов."
  network_id  = yandex_vpc_network.this.id

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает подключение к узлам по SSH с указанных IP-адресов."
    v4_cidr_blocks = concat(["10.0.0.0/8", "192.168.0.0/16", "172.16.0.0/12", "85.32.32.22/32"], var.office_external_ips)
    port           = 22
  }
}

resource "yandex_vpc_security_group" "k8s-master-whitelist" {
  name = "otus-md-fn-${var.env}-sg-k8s-master-whitelist"
  network_id = yandex_vpc_network.this.id
  description = "Правила группы разрешают доступ к Kubernetes API из интернета. Примените правила только к кластеру."

  ingress {
    protocol        = "TCP"
    description     = "Правило разрешает подключение к Kubernetes API через порт 6443 из указанной сети."
    v4_cidr_blocks  = concat(["192.168.0.0/24", "85.23.23.22/32"], var.office_external_ips, var.k8s_master_whitelist)
    port = 6443
  }

  ingress {
    protocol        = "TCP"
    description     = "Правило разрешает подключение к Kubernetes API через порт 443 из указанной сети."
    v4_cidr_blocks  = concat(["192.168.0.0/24", "85.23.23.22/32"], var.office_external_ips, var.k8s_master_whitelist)
    port = 443
  }
}

/**
* Service account - Сервисный аккаунт
*/
resource "yandex_iam_service_account" "this" {
  name = "otus-md-fn-sa-${var.env}"
  description = "Service account for manage otus-md-fn-${var.env} k8s cluster"
}

/**
* Добавляем роли для сервисного аккаунта, необходимые для создания кластера
*
* !!!Примечание: права выдаются с некоторой задержкой, поэтому следует в соответствующих ресурсах (в данном примере это yandex_resourcemanager_folder_iam_member), добавить sleep_after = 30s.
* Иначе, при удалении ресурсов, Terraform удалит одновременно кластер и права доступа для учетных записей служб. Это может вызвать проблемы при удалении кластера и связанных с ним групп узлов.
*/
resource yandex_resourcemanager_folder_iam_member "editor" {
  folder_id = var.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
  role      = "editor"
  sleep_after = 30
}
resource yandex_resourcemanager_folder_iam_member "images-puller" {
  folder_id = var.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
  role      = "container-registry.images.puller"
  sleep_after = 30
}

resource yandex_kubernetes_node_group "this" {
  cluster_id = yandex_kubernetes_cluster.regional_cluster_for_functions.id
  name = "otus-md-fn-node-group-${var.env}"
  description = "Node group for otus-md-fn-${var.env}"
  version = var.k8s_version
  instance_template {
    platform_id = var.k8s_node_platform

    network_interface {
      nat = false // TechSupport: Указанный способ именно добавляет публичные адреса к нодам с помощью технологии one-to-one NAT
      ipv4 = true
      subnet_ids = [yandex_vpc_subnet.subnet_b.id]
      security_group_ids = [
        yandex_vpc_security_group.k8s-main-sg.id,
        yandex_vpc_security_group.k8s-public-services.id,
        yandex_vpc_security_group.k8s-nodes-ssh-access.id
      ]
    }

    resources {
      memory = var.k8s_node_memory_size
      cores = var.k8s_node_cpu_cores
      core_fraction = var.k8s_node_cpu_core_fraction
    }

    boot_disk {
      size = var.k8s_node_disk_size
      type = "network-hdd"
    }

    scheduling_policy {
      preemptible = false
    }
  }

  scale_policy {
    auto_scale {
      initial = var.k8s_node_min_count
      max     = var.k8s_node_max_count
      min     = var.k8s_node_min_count
    }
  }

  // For regional k8s cluster. Auto scale node groups can have only one location
  allocation_policy {
    location {
      zone = yandex_vpc_subnet.subnet_b.zone
    }
#    location {
#      zone = "${yandex_vpc_subnet.subnet_b.zone}"
#    }
#    location {
#      zone = "${yandex_vpc_subnet.subnet_c.zone}"
#    }
  }

  maintenance_policy {
    auto_repair  = true
    auto_upgrade = true
  }

  deploy_policy {
    max_expansion   = 3
    max_unavailable = 0
  }
}

locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${yandex_kubernetes_cluster.regional_cluster_for_functions.master[0].external_v4_endpoint}
    certificate-authority-data: ${base64encode(yandex_kubernetes_cluster.regional_cluster_for_functions.master[0].cluster_ca_certificate)}
  name: md-fn-k8s-${var.env}-eu
contexts:
- context:
    cluster: md-fn-k8s-${var.env}-eu
    user: yc
  name: md-fn-${var.env}-eu
current-context: md-fn-${var.env}-eu
users:
- name: yc
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: yc
      args:
      - k8s
      - create-token
KUBECONFIG
}
output "kubeconfig" {
  value = local.kubeconfig
}

