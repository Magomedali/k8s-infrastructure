terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.69.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region     = var.aws_region
  shared_credentials_file = var.shared_credentials_file
  profile                 = var.profile
}

locals {
  cluster_name = "otus-${var.env}-${var.md_region}"
  cluster_suffix = "${var.env}-${var.md_region}"
}

##
# Создание виртуальной сети
##
resource "aws_vpc" "virtual_private_cloud" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = tomap({
    "Name": "otus-vpc-${local.cluster_suffix}",
    "kubernetes.io/cluster/${local.cluster_name}": "shared",
  })
}

data "aws_availability_zones" "available" {}

##
# Создание подсетей
##
resource "aws_subnet" "vpc_subnet" {
  count = 3

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.virtual_private_cloud.id

  tags = tomap({
    "Name"                                      = "otus-subnet-${local.cluster_suffix}-${count.index}",
    "kubernetes.io/cluster/${local.cluster_name}" = "shared",
  })
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.virtual_private_cloud.id

  tags = {
    Name = "otus-igw-${local.cluster_name}"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.virtual_private_cloud.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "route_table_association" {
  count = 3

  subnet_id      = aws_subnet.vpc_subnet.*.id[count.index]
  route_table_id = aws_route_table.route_table.id
}

##
# Create iam role for EKS
# https://registry.terraform.io/providers/hashicorp/aws/2.34.0/docs/guides/eks-getting-started#kubernetes-masters
# https://docs.aws.amazon.com/eks/latest/userguide/service_IAM_role.html
##
resource "aws_iam_role" "iam_role" {
  name = "otus-iam-role-${local.cluster_suffix}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "demo-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.iam_role.name
}

resource "aws_iam_role_policy_attachment" "demo-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.iam_role.name
}

resource "aws_security_group" "otus-fn-sg-default" {
  name        = "otus-sg-${local.cluster_suffix}"
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.virtual_private_cloud.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "otus-sg-${local.cluster_suffix}"
  }
}

# OPTIONAL: Allow inbound traffic from your local workstation external IP
#           to the Kubernetes. You will need to replace A.B.C.D below with
#           your real IP. Services like icanhazip.com can help you find this.
resource "aws_security_group_rule" "otus-cluster-ingress-workstation-https" {
  cidr_blocks       = concat(var.office_external_ips, var.k8s_master_whitelist)
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.otus-fn-sg-default.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_eks_cluster" "otus-cluster" {
  name            = local.cluster_name
  role_arn        = aws_iam_role.iam_role.arn
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  vpc_config {
    security_group_ids = [aws_security_group.otus-fn-sg-default.id]
    subnet_ids         = flatten([aws_subnet.vpc_subnet.*.id])
    public_access_cidrs = concat(var.office_external_ips, var.k8s_master_whitelist)
    endpoint_private_access = true
    endpoint_public_access = true
  }

  tags = {
    "billing-group" = "otus-${var.env}"
  }

  tags_all = {
    "billing-group" = "otus-${var.env}"
  }
  version = var.k8s_version

  depends_on = [
    aws_iam_role_policy_attachment.demo-cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.demo-cluster-AmazonEKSServicePolicy,
  ]
}

##
# Worker Node IAM Role and Instance Profile
# The below is an example IAM role and policy to allow the worker nodes to manage or retrieve data from other AWS services. It is used by Kubernetes to allow worker nodes to join the cluster
##

resource "aws_iam_role" "iam_role_node" {
  name = "otus-iam-role-node-${local.cluster_suffix}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "iam_role_node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.iam_role_node.name
}

resource "aws_iam_role_policy_attachment" "iam_role_node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.iam_role_node.name
}

resource "aws_iam_role_policy_attachment" "iam_role-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.iam_role_node.name
}

resource "aws_iam_instance_profile" "iam_instance_profile-otus" {
  name = "iam_instance_profile-${local.cluster_suffix}"
  role = aws_iam_role.iam_role_node.name
}

##
# Worker Node Security Group
# This security group controls networking access to the Kubernetes worker nodes.
##
resource "aws_security_group" "otus-fn-sg-node" {
  name = "otus-sg-node-${local.cluster_suffix}"
  description = "Security group for all nodes in the cluster"
  vpc_id      = aws_vpc.virtual_private_cloud.id

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "otus-sg-node-${local.cluster_suffix}"
    "kubernetes.io/cluster/${local.cluster_name}" = "owned"
  }
}

resource "aws_security_group_rule" "otus-fn-sg-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.otus-fn-sg-node.id
  source_security_group_id = aws_security_group.otus-fn-sg-node.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "otus-fn-sg-node-ingress-cluster-https" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.otus-fn-sg-node.id
  source_security_group_id = aws_security_group.otus-fn-sg-default.id
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_security_group_rule" "otus-fn-sg-node-ingress-cluster-others" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.otus-fn-sg-node.id
  source_security_group_id = aws_security_group.otus-fn-sg-default.id
  to_port                  = 65535
  type                     = "ingress"
}

##
# Worker Node Access to EKS Master Cluster
##
resource "aws_security_group_rule" "otus-fn-sg-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.otus-fn-sg-default.id
  source_security_group_id = aws_security_group.otus-fn-sg-node.id
  to_port                  = 443
  type                     = "ingress"
}

##
# Worker Node AutoScaling Group
##
data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = [var.aws_ami_name]
  }

  most_recent = true
  owners      = [var.ami_account_id] # Amazon EKS AMI Account ID
}

# This data source is included for ease of sample architecture deployment
# and can be swapped out as necessary.
data "aws_region" "current" {}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  otus-fn-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.otus-cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.otus-cluster.certificate_authority.0.data}' '${local.cluster_name}'
USERDATA
}

resource "aws_launch_template" "otus-fn-launch_template-m5-large" {
  name = "otus-fn-launch_template-${local.cluster_suffix}-m5-large"

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size = 64
      volume_type = "gp2"
      delete_on_termination = true
    }
  }
  iam_instance_profile {
    arn = aws_iam_instance_profile.iam_instance_profile-otus.id
  }

  image_id                    = data.aws_ami.eks-worker.id
  instance_type               = var.node_instance_type

  network_interfaces {
    associate_public_ip_address = true
    security_groups = [aws_security_group.otus-fn-sg-node.id]
  }
  # Issue - https://github.com/hashicorp/terraform-provider-aws/issues/4570
  #vpc_security_group_ids = [aws_security_group.otus-fn-sg-node.id]
  user_data            = base64encode(local.otus-fn-node-userdata)
  key_name = var.ssh_key_pair_name
  tags = {
    "eks:cluster-name": local.cluster_name
    "eks:nodegroup-name": "otus-fn-node-group-${local.cluster_suffix}"
  }
}

resource "aws_autoscaling_group" "otus-fn-asg-m5-large" {
  launch_template {
    id = aws_launch_template.otus-fn-launch_template-m5-large.id
    version = aws_launch_template.otus-fn-launch_template-m5-large.latest_version
  }
  max_size             = var.k8s_node_max_count
  min_size             = var.k8s_node_min_count
  desired_capacity     = var.k8s_node_min_count
  name                 = "${local.cluster_name}-asg-node-m5-large"
  vpc_zone_identifier  = flatten([aws_subnet.vpc_subnet.*.id])

  tag {
    key                 = "Name"
    value               = "asg-${local.cluster_name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${local.cluster_name}"
    value               = "owned"
    propagate_at_launch = true
  }
}

resource "aws_eks_node_group" "otus-fn-node-group-m5-large" {
  cluster_name    = local.cluster_name
  node_group_name = "otus-fn-node-group-${local.cluster_suffix}-m5-large"
  node_role_arn   = aws_iam_role.iam_role_node.arn
  subnet_ids      = aws_subnet.vpc_subnet[*].id

  scaling_config {
    desired_size = var.k8s_node_min_count
    max_size     = var.k8s_node_max_count
    min_size     = var.k8s_node_min_count
  }

  tags = {
    "billing-group" = "otus-${var.env}"
  }

  tags_all = {
    "billing-group" = "otus-${var.env}"
  }

  ami_type = var.aws_ami_type
  instance_types = [var.node_instance_type]
  disk_size = 64
  update_config {
    max_unavailable = 1
  }

  remote_access {
    ec2_ssh_key = var.ssh_key_pair_name
    source_security_group_ids = [aws_security_group.otus-fn-sg-node.id]
  }
  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.iam_role_node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.iam_role_node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.iam_role-AmazonEC2ContainerRegistryReadOnly,
  ]
}

locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.iam_role_node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH
}

output "config_map_aws_auth" {
  value = local.config_map_aws_auth
}